%global __requires_exclude /bin/awk

%global _default_patch_fuzz 2

Summary: 	Dynamic Kernel Module Support Framework
Name: 		dkms
Version: 	2.0.19
URL:		http://linux.dell.com/dkms
Release: 	%mkrel 39
License: 	GPL
Group:  	System/Base
BuildArch: 	noarch
Requires:	kernel-devel
Recommends:	kernel-devel-latest
Requires:	%{name}-minimal = %{version}-%{release}
Requires(pre):	rpm-helper
Requires(post):		rpm-helper >= %{rpmhelper_required_version}
Requires(preun):	rpm-helper >= %{rpmhelper_required_version}
Requires:	patch
Source:		http://linux.dell.com/dkms/%{name}-%{version}.tar.gz
Source1:	template-dkms-mkrpm.spec
Source2:	dkms.depmod.conf
Source3:	autoload.awk

## (ngompa): This is an analysis of the patches for DKMS.
## Unless otherwise noted, I did not write or rediff the patches

# Do not depend on kernel rpm version for identifying kernel
# This is necessary as Mageia kernels do not currently have the real version-release
# in the version and release fields of the RPM data
Patch1:		dkms-2.0.19-norpm.patch
# Add support for identifying the various kernel flavors Mageia offers
# This can probably be simplified soon, as we don't have nearly this many flavors anymore...
Patch2:		dkms-2.0.19-mgaify.patch
# Change prepare-all to prepare invocations
# Not sure why this is needed...
Patch3:		dkms-fix-kernel-make-prepare.patch
# Compress ko files with gzip
Patch4:		dkms-2.0.17.6-compressed-module.patch
# Alters the form of the logic for module identification
# This can be dropped, as the logic is now completely different
Patch5:		dkms-2.0.19-weak_module_name.patch
# More kernel flavor support code...
Patch7:		dkms-2.0.19-procconfig.patch
# More kernel flavor support code...
Patch8:		dkms-2.0.19-mdkrpm-split-ver-rel.patch
# More complete bash completion
# This looks upstremable...
Patch9:		dkms-2.0.19-bash-completion-update.patch
# Don't copy over the SRPMs, we don't care about them...
Patch10:	dkms-2.0.19-binary_only.patch
# Add support for declaring minimum and maximum supported kernels to build with
Patch11:	dkms-2.0.17.5-min-max-kernel.patch
# Test for the existence of dkms.conf
Patch12:	dkms-2.0.17.6-test-dkms.conf-existence.patch
# Add local variables for module and module verion for status
# Does this even do anything? It doesn't seem needed...
Patch13:	dkms-2.0.17.6-status_default.patch
# Change output from /dev/console to /dev/stdout for autoinstaller
# This can be dropped, as the logic is completely different now...
Patch14:	dkms-2.0.17.6-stdout.patch
# Drop overrides to use external dependency generator
# This is probably still needed...
Patch15:	dkms-2.0.19-no_custom_rpm_provides.patch
# Add binary module installation type/status
Patch16:	dkms-2.0.19-binary.patch
# Autogenerate modaliases
Patch17:	dkms-2.0.19-autoalias.patch
# Ensure that make_rpm() function exits with success
Patch18:	dkms-2.0.19-mkrpm_status.patch
# Deletes a check that doesn't seem to be used...
Patch19:	dkms-2.0.19-skip-unused-check.patch
# Speed up module removal
Patch20:	dkms-2.0.19-uninstall-speedup.patch
# mark dkms_autoinstaller as X-Mandriva-Interactive
# This can be dropped as we moved to systemd a long time ago
Patch21:	dkms-2.0.19-init-mdv-interactive.patch
# Populate module symvers
Patch22:	dkms-symvers.patch
# Switch from udev trigger to dkms_autoload
# This may be able to be dropped...
Patch23:	dkms-2.0.19-autoload_instead_of_udevadm.patch
# Cosmetic patch to suggest "kernel devel" instead of "kernel source"
# This should be dropped
Patch24:	dkms-2.0.19-suggest-devel-not-source.patch
# Add support for making xz compressed modules
Patch25:	dkms-2.0.19-xz-support.patch
# (doktor5000) remove explicit usage of " > /dev/stdout" as standard output
# this breaks systemd logging and is unnecessary for standard output
Patch26:       dkms-2.0.19-mga-remove_dev_stdout_output_loc.patch
# Enable parallel build of kernel modules
Patch27:	dkms-2.0.19-parallel-build.patch
# Don't load modules automatically while installer is running (mga#20368)
Patch28:	dkms-no-autoload-during-install.patch
# Display text while building+installing driver through Plymouth
Patch29:	dkms-display-bootsplash-message.patch

%define _dkmsdir %{_localstatedir}/lib/%{name}
%define _dkmsbinarydir %{_localstatedir}/lib/%{name}-binary

%description
This package contains the framework for the Dynamic
Kernel Module Support (DKMS) method for installing
module RPMS as originally developed by the Dell
Computer Corporation.

This package is intended for building binary kernel
modules with dkms source packages installed

%package minimal
Summary: 	Dynamic Kernel Module Support Framework - minimal package
License: 	GPL
Group: 		System/Base
Requires:	lsb-release
Requires(preun):	rpm-helper
Requires(post):	rpm-helper

%description minimal
This package contains the framework for the Dynamic
Kernel Module Support (DKMS) method for installing
module RPMS as originally developed by the Dell
Computer Corporation.

This package is intended for installing binary module RPMS
as created by dkms.

%prep
%setup -q
%patch1 -p1 -b .norpm
%patch2 -p1 -b .mdkize
%patch3 -p1 -b .fix-kernel-make-prepare
%patch4 -p1 -b .compressed-module
%patch5 -p1 -b .weak_module_name
%patch7 -p1 -b .procconfig
%patch8 -p1 -b .mdkrpm-split-ver-rel
%patch9 -p1 -b .bash-completion-update
%patch10 -p1 -b .binary_only
%patch11 -p1 -b .min-max-kernel
%patch12 -p1 -b .test-dkmsconf
%patch13 -p1 -b .status_default
%patch14 -p1 -b .stdout
%patch15 -p1 -b .no_custom_rpm_provides
%patch16 -p1 -b .binary
%patch17 -p0 -b .autoalias
%patch18 -p1 -b .mkrpm
%patch19 -p1 -b .versionsanity
%patch20 -p1 -b .uninst-speedup
%patch21 -p1 -b .mdv-interactive
%patch22 -p1 -b .symvers
%patch23 -p1 -b .autoload_instead_of_udevadm
%patch24 -p1 -b .suggest-devel
%patch25 -p1 -b .xz-support
%patch26 -p1 -b .dev_stdout_output_loc
%patch27 -p1 -b .parallel-build
%patch28 -p1 -b .no-autoload-during-install
%patch29 -p1 -b .display-bootsplash-message

sed -i -e 's,/var/%{name},%{_dkmsdir},g;s,init.d/dkms_autoinstaller,init.d/%{name},g' \
  dkms_autoinstaller \
  dkms_framework.conf \
  kernel_*.d_dkms \
  %{name}.8 \
  dkms

%install
mkdir -p %{buildroot}%{_mandir}/man8
%makeinstall_std INITD=%{buildroot}%{_initrddir}
install -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/%{name}/
install -m 755 dkms_mkkerneldoth %{buildroot}/%{_sbindir}/dkms_mkkerneldoth
install -m 755 %{SOURCE3} %{buildroot}/%{_sbindir}/dkms_autoload
mv %{buildroot}%{_initrddir}/dkms_autoinstaller %{buildroot}%{_sbindir}
mkdir -p %{buildroot}%{_dkmsbinarydir}
mkdir -p %{buildroot}%{_sysconfdir}/depmod.d
install -m644 %{SOURCE2} %{buildroot}%{_sysconfdir}/depmod.d/%{name}.conf


# (doktor5000) systemd service file to run dkms_autoinstaller on every boot
# necessary to rebuild dkms modules for kernels booting the first time
mkdir -p %{buildroot}%{_unitdir}
cat << 'EOF' > %{buildroot}%{_unitdir}/%{name}-autorebuild.service
[Unit]
Description=run dkms_autoinstaller on every boot to rebuild dkms modules for newly booted kernels
DefaultDependencies=no
Conflicts=shutdown.target
Before=basic.target shutdown.target
After=local-fs.target

[Service]
ExecStart=%{_sbindir}/dkms-autorebuild.sh
Type=oneshot
RemainAfterExit=yes

[Install]
Alias=dkms_autoinstaller.service
WantedBy=basic.target

EOF


# (doktor5000) split out from /etc/init.d/mandrake_everytime / mandriva-everytime.service
# triggered by above systemd service on every boot (mga#
cat << 'EOF' > %{buildroot}%{_sbindir}/%{name}-autorebuild.sh
#!/bin/sh

# start dkms module build for newly booted kernels via %{name}-autorebuild.service
# rebuild can be disabled by setting DKMS_ONBOOT=no in /etc/sysconfig/system

[ -r /etc/sysconfig/system ] && . /etc/sysconfig/system

DKMS_TOOL=%{_sbindir}/dkms_autoinstaller
if [ "$DKMS_ONBOOT" != "no" ] && [ -x $DKMS_TOOL ]; then
# dkms_autoinstaller evaluates $verbose so enable it here for complete logging
    verbose=yes $DKMS_TOOL start
fi
EOF

chmod 755 %{buildroot}%{_sbindir}/%{name}-autorebuild.sh

%define _dkms_autorebuild_installed %{_localstatedir}/lib/rpm-state/dkms-autorebuild-installed

%pre
if [ $1 -eq 2 ] ; then
    if [ -f %{_unitdir}/%{name}-autorebuild.service ] ; then
        touch %{_dkms_autorebuild_installed}
    fi
fi

%post
if [ -f %{_dkms_autorebuild_installed} ] ; then
    # We are upgrading from a version that included the dkms-autorebuild
    # service.
    rm %{_dkms_autorebuild_installed}
else
    # We are either installing or upgrading from a version that didn't
    # include the dkms-autorebuild service. Force it to be installed.
    set -- 1
fi
%_post_service %{name}-autorebuild

%preun
%_preun_service %{name}-autorebuild

%triggerpostun -- dkms < 2.0.19-11
rm -f /etc/rc.d/*/{K,S}??dkms

%files
%doc %attr (-,root,root) sample.spec sample.conf AUTHORS COPYING template-dkms-mkrpm.spec 
%{_sbindir}/dkms_autoinstaller
%{_unitdir}/%{name}-autorebuild.service
%{_sbindir}/%{name}-autorebuild.sh

%files minimal
%{_sbindir}/dkms
%{_dkmsdir}
%dir %{_dkmsbinarydir}
%{_sbindir}/dkms_mkkerneldoth
%{_sbindir}/dkms_autoload
%{_mandir}/man8/dkms.8*
%config(noreplace) %{_sysconfdir}/dkms
# these dirs are for plugins - owned by other packages
%{_sysconfdir}/kernel/postinst.d/%{name}
%{_sysconfdir}/kernel/prerm.d/%{name}
%{_sysconfdir}/bash_completion.d/%{name}
%{_sysconfdir}/depmod.d/%{name}.conf
